package ReportCard;

import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class GradingApp {
    public static void main(String[] args) {


        int[]Grades1S = {98, 58, 90};
        int[]Grades2S = {70, 90, 95};
        int[]Grades3S = {90, 95, 100};
        int[]Grades4S = {97, 100, 100};


        Student Eric = new Student("Eric");
        for (int x = 0; x < Grades1S.length; x++){
            Eric.addGrade(Grades1S[x]);
        }

        Student Jose = new Student("Jose");
        for (int x = 0; x < Grades2S.length; x++){
            Jose.addGrade(Grades2S[x]);
        }

        Student Nick = new Student("Nick");
        for (int x = 0; x < Grades3S.length; x++){
            Nick.addGrade(Grades3S[x]);
        }

        Student Stephen = new Student("Stephen");
        for (int x = 0; x < Grades4S.length; x++){
            Stephen.addGrade(Grades4S[x]);
        }


        HashMap<String, Student> Students = new HashMap<>();
        Students.put("Student1", Eric);
        Students.put("Student2", Jose);
        Students.put("Student3", Nick);
        Students.put("Student4", Stephen);


//        Print the list of gitlab usernames out to the console, and ask the user which student they would like to see more information about.
//        The user should enter a github username (i.e. one of the keys in your map). If the given input does not match up with a key in your map,
//        tell the user that no users with that username were found.
//        If the given username does exist, display information about that student, including their name and their grades.
//                After the information is displayed, the application should ask the user if they want to continue, and keep running
//                so long as the answer is yes.



        boolean repeat = true;
       /* boolean dontRepeat = false;*/

       do {


            System.out.println("Type in the Username of a specific student for more information");
            System.out.println("Student1");
            System.out.println("Student2");
            System.out.println("Student3");
            System.out.println("Student4");


        Scanner sc = new Scanner(System.in);
           String userInput = sc.nextLine();

            if (userInput.equals("Student1")) {
                System.out.println("Student 1 Information");
                System.out.println(Students.get("Student1").getName());
                System.out.println(Students.get("Student1").getGradeAverage());
                System.out.println("Would you like to continue? Y / N");
                String userInput2 = sc.nextLine().toUpperCase();


                if (userInput2.equals("N")){
                    repeat = false;
                }
                else if(userInput2.equals("Y")){
                    repeat = true; }
                else System.out.println("Unknown error");
            }




            if (userInput.equals("Student2")) {
                System.out.println("Student 2 Information");
                System.out.println(Students.get("Student2").getName());
                System.out.println(Students.get("Student2").getGradeAverage());
                System.out.println("Would you like to continue? Y / N");
                String userInput2 = sc.nextLine();


                if (userInput2.equals("Y")){
                    repeat = true; }
                if (userInput2.equals("N")){
                    repeat = false;
                } else System.out.println("Unknown error");

            }




            if (userInput.equals("Student3")) {
                System.out.println("Student 3 Information");
                System.out.println(Students.get("Student3").getName());
                System.out.println(Students.get("Student3").getGradeAverage());
                System.out.println("Would you like to continue? Y / N");
                String userInput2 = sc.nextLine();


                if (userInput2.equals("Y")){
                    repeat = true; }
                if (userInput2.equals("N")){
                    repeat = false;
                } else System.out.println("Unknown error");
            }




            if (userInput.equals("Student4")) {
                System.out.println("Student 4 Information");
                System.out.println(Students.get("Student4").getName());
                System.out.println(Students.get("Student4").getGradeAverage());
                System.out.println("Would you like to continue? Y / N");
                String userInput2 = sc.nextLine();

                    if (userInput2.equals("Y")) {
                        repeat = true;
                    }
                    if (userInput2.equals("N")) {
                        repeat = false;
                    } else System.out.println("Unknown error");

            }

        } while (repeat);

        System.out.println("You have successfully quit, logging off");
    }
}
