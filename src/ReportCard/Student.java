package ReportCard;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Student {

    private String name;
    private List<Integer> grades;


    public Student(String name) {
        this.name = name;
        this.grades = new ArrayList<>();

    }


    public String getName() {
                return name;

    }
    // which returns the student's name

    public void addGrade(int grade) {
        this.grades.add(grade);

    }

    // adds the given grade to the grades property


    public double getGradeAverage() {
        int average = 0;
        for (int grade : this.grades) {
            average += grade;
        }
        return  (double) average / this.grades.size();
    }
// which returns the average of the students grades
public static void main (String[] args) {
Student s1 = new Student("reed");

s1.addGrade(80);
s1.addGrade(100);

    System.out.println(s1);





}
}
