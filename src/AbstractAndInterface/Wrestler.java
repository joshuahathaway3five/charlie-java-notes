package AbstractAndInterface;

public abstract class Wrestler {

    // METHOD
    public static void paymentForPerformance(int hours) {
        System.out.println("The Wrestler's pay for tonight's performance: " + 250 * hours);
    }


    // ABSTRACT METHOD
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisher();


}
