package AbstractAndInterface;

public class JohnCena extends Wrestler{
    @Override
    public void wrestlerName() {
        System.out.println("John Cena");
    }

    @Override
    public void themeMusic() {
        System.out.println("You can't see me");
    }

    @Override
    public void finisher() {
        System.out.println("Attitude Adjustment");
    }

//    public void paymentForPerformance(int hours) {
//        System.out.println("John Cena pay" + hours*300);
//    }

}
