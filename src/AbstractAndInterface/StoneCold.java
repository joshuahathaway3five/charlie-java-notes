package AbstractAndInterface;

// extends for class (e c)
//implements for interface (i for an i)

public class StoneCold implements WWE {
    @Override
    public void wrestlerName() {
        System.out.println("Stone Cold Steve Austin");
    }

    @Override
    public void themeMusic() {
        System.out.println("Shattered Glass");
    }

    @Override
    public void finisher() {
        System.out.println("Stone Cold Stunner");
    }

    @Override
    public void paymentForPerformance(int hours) {
        System.out.println("Stone Cold's pay" + 500 * hours);
    }
}
