package AbstractAndInterface;

// Intereface - much like abstract class in that they cannot be instantiated
// instead must be implemented by classes or extended by other interfaces
//contains only abstract methods ***
//an interface is a special case of an abstract class


public interface WWE {
    // what if a wrestler has a bigger pay?

    //abstract methods
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisher();

    public abstract void paymentForPerformance(int hours);


}
