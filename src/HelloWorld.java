public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("Welcome To Java");

//        single line comment

        /*
        multi
        line
        comment
         */

        //DATA TYPES
        /*
        byte - very short integers from -128 to 127
        short - short integers from -32,768 to 32,767
        int - integers
         */


        /*
        Strings
        "string / long sentences"
        'C'
        */

        System.out.println("Escape charecters: ");
        System.out.println("First \\");
        System.out.println("Second \n");
        System.out.println("Third \t");

        // Variables
        /*
        all variables in Java must be declared before they are used
        SYNTAX
        dataType nameOfVariable;*/

        byte age = 12;
        short myShort = -32768;
//        int myInteger;

//        myInteger = 18;
//        System.out.println(myInteger);

       boolean userLoggedIn = false;
        System.out.println(userLoggedIn);

        int num1;
        int num2;
        num1 = 12;
        num2 = 13;

        int sum = num1 + num2;
        System.out.println(sum);

        // Casting
        //turning a value of one type into another
        //Two types of casting
        //IMPLICIT / EXPLICIT casting

        //IMPLICIT CASTING
        // - involves assigning a value of a less precise data type to a variable whose types is of a higher precision

        int myInteger = 900;
        long morePrecise = myInteger;
        System.out.println(morePrecise);

        // EXPLICIT CASTING
        double pi = 3.14159;
        int almostPi = (int) pi;
        System.out.println(almostPi);


    }
}
