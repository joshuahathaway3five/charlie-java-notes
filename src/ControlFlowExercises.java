import java.util.Scanner;

public class ControlFlowExercises {
    public static void main(String[] args) {

//        Loop Basics
//        While
//                * Create an integer variable i with a value of 9.
//                * Create a while loop that runs so long as i is less than or equal to 23
//                * Each loop iteration, output the current value of i, then increment i by one.
//        Your output should look like this:
//        9 10 11 12 13 14 15 16 17 18 19 20 21 22 23

        int i = 9;
        while (i <= 23) {
            System.out.print(i + " ");
            i++;
        }
//
//        Do While
//        * Create a do-while loop that will count by 2's starting with 0 and ending at 100. Follow each number with a new line.
//                * Alter your loop to count backwards by 5's from 100 to -10.
//                * Create a do-while loop that starts at 2, and displays the number squared on each line while the number is less than 1,000,000. Output should equal:
//        2
//        4
//        16
//        256
//        65536

//        int x = 0;
//        do {
//            System.out.println(x);
//            x += 2;
//        }
//        while (x <= 100);

//        int x = 100;
//        do {
//            System.out.println(x);
//            x -= 5;
//        }
//        while (x >= -10);

//        long x = 2;
//        do {
//            System.out.println(x);
//            x *= x;
//        }
//        while (x < 1000000);


//        Fizzbuzz
//        One of the most common interview questions for entry-level programmers is the FizzBuzz test. Developed by Imran Ghory, the test is designed to test basic looping and conditional logic skills.
//                - Write a program that prints the numbers from 1 to 100.
//                - For multiples of three print "Fizz" instead of the number
//        - For the multiples of five print "Buzz".
//                - For numbers which are multiples of both three and five print "FizzBuzz".


        for (var a = 1; a <= 100; a++) {
            if (a % 3 == 0 && a % 5 == 0) {
                System.out.println("FIZZBUZZ");
            }
            else if (a % 3 == 0) {
                System.out.println("FIZZ");
            }
            else if (a % 5 == 0) {
                System.out.println("BUZZ");
            }
            else {
                System.out.println(a);
            }
        }

Scanner scanner = new Scanner(System.in);

        String choice = "y";
        while(choice.equalsIgnoreCase("y")) {
            System.out.println("What number would you like to go up to?: ");
            int integerNext = scanner.nextInt();
            System.out.println("Here is your table");
            System.out.println("Number" + " | " + "Squared" + " | " + "Cubed");
            System.out.println("======" + " | " + "======" + "  | " + "======");
            for(int R = 1; R <= integerNext; R++) {
                int numberSquared = (int) Math.pow(R, 2);
                int numberCubed = (int) Math.pow (R, 3);
                String message = "\t" + R + "  |  \t" + numberSquared + "    |   \t" + numberCubed;
                System.out.println(message);
//                System.out.format("%-6d | %-6d | %-6d%n", i, numberSquared, numberCubed);
            }
            System.out.print("Continue? (y/n): ");
            choice = scanner.next();
            System.out.println();
        }



//        Display a table of powers.
//                - Prompt the user to enter an integer.
//        - Display a table of squares and cubes from 1 to the value entered.
//        - Ask if the user wants to continue.
//        - Assume that the user will enter valid data.
//                - Only continue if the user agrees to.
//        number | squared | cubed
//                ------ | ------- | -----
//                1      | 1       | 1
//        2      | 4       | 8
//        3      | 9       | 27
//        4      | 16      | 64
//        5      | 25      | 125


        Scanner Input = new Scanner(System.in);
        boolean end = false;
        while (end == false){
            System.out.println("input grade (range: 0-100)");
            int grade = Input.nextInt();
            String letter;
            letter = "I'm going to initialize it later stop throwing an error";
            if (grade <= 100 && grade >= 90){
                letter = "A";
            }
            if (grade <= 89 && grade >= 80){
                letter = "B";
            }
            if (grade <= 79 && grade >= 70){
                letter = "C";
            }
            if (grade <= 69 && grade >= 60){
                letter = "D";
            }
            if (grade <= 59){
                letter = "F";
            }
            String bonus = Integer.toString(grade);
            String gradeFinal;
            if (bonus.endsWith("9") || bonus.endsWith("8") || bonus.equals("100")){
                gradeFinal = letter + "+";
            } else
            if (bonus.endsWith("3") || bonus.endsWith("2") || bonus.endsWith("1") || bonus.endsWith("0")){
                gradeFinal= letter + "-";
            } else {
                gradeFinal = letter;
            }
            switch (gradeFinal) {
                case "A+":
                    System.out.println("Grade: " + grade + " is an " + gradeFinal);
                    break;
                case "A":
                    System.out.println("Grade: " + grade + " is an " + gradeFinal);
                    break;
                case "A-":
                    System.out.println("Grade: " + grade + " is an " + gradeFinal);
                    break;
                case "B+":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "B":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "B-":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "C+":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "C":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "C-":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "D+":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "D":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "D-":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "F":
                    System.out.println("Failure!");
                    break;
            }
            System.out.println("\nEnter another grade(Y/N)?:");
            String checkEnd = Input.next();
            checkEnd = checkEnd.toUpperCase();
            if (checkEnd.startsWith("N")){
                end = true;
            }
        }

    }
}
