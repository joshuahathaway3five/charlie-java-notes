package CollectionsExercises;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {

        List<Integer> userAgeList = new ArrayList<>();

                userAgeList.add(40);
                userAgeList.add(53);
                userAgeList.add(45);
                userAgeList.add(53);

        System.out.println(userAgeList);

        userAgeList.add(2, 53);

        System.out.println(userAgeList);

        userAgeList.set(3, 49);

        System.out.println(userAgeList);

        userAgeList.remove(3);

        System.out.println(userAgeList);

        userAgeList.get(2);

        System.out.println(userAgeList);

        System.out.println(userAgeList.size());

        System.out.println(userAgeList.contains(12));


    }
}
