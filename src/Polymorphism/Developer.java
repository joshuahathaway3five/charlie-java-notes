package Polymorphism;

public class Developer {

//    public String work() {
//        return " I'm coding from the office";
//
//    }

    // - prevents ScrumMaster class from INHERITING the work
    public String work() {
        return "I'm NOT coding, but I'm at home ";
    }
}
