package Collections;

//Collections - Data structure that can be used to group, or collect objects
//The java standard library, a collection of code that comes with the java language,
//contains many collections to help with common programming tasks.

// Main Focus:
// Array List
// HashMaps

// ArrayList
// - represents an array that can change its size



// .size
// .add
//.get
//.indexof
//.contain
//.lastindexof
//.isEmpty
//.remove


// Syntax
// ArrayList<Type> name = new ArrayList<>();

// HASH MAP
// - data structure for keyvalue pairs
// - implemented by using the hashmap class in java
// - very similar to object in JS
// - all of the keys in theHASH MAP must be of the same type and all the values


//.PUT
//.get
//.getordefualt
//.containskey
//.containsvalue
//.putifabsent
//.remove
//.replace
//.clear
//.isEmpty


import java.util.*;

public class Main {
    public static void main(String[] args) {
        // arraylist examples

        ArrayList<Integer> numbers = new ArrayList<>(); // created an array of integers named numbers

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(25);

        System.out.println();

        ArrayList<String> names = new ArrayList<>(Arrays.asList(
                "Pop",
                "Duncan",
                "Parker",
                "Ginobili"
        ));

        names.add("Robinson");
        System.out.println(names);

        ArrayList<Integer> myNumbers = new ArrayList<>();

        myNumbers.add(23);
        myNumbers.add(50);
        myNumbers.add(32);
        myNumbers.add(91);

        // peeling into the list
        int num = myNumbers.get(0); //23
        int myNum = myNumbers.get(2); // 32
        System.out.println(num);
        System.out.println(myNum);


        // edit elements
        myNumbers.set(3, 100);
        System.out.println(myNumbers); // 91 -> 100

        //remove elements
        myNumbers.remove(2); // removes 32 from myNumbers array
        System.out.println(myNumbers);

        // reordering list
        Collections.sort(myNumbers);
        Collections.reverse(myNumbers);
        System.out.println(myNumbers);

        ArrayList<String> roasts = new ArrayList<>(Arrays.asList(
                "light",
                "medium",
                "bold",
                "dark"

        ));
        System.out.println(roasts.contains("espresso"));  // false
        System.out.println(roasts.lastIndexOf("bold")); // 2
        System.out.println(roasts.isEmpty());  // false
        System.out.println(roasts.indexOf("medium")); // 1


        // HASHMAP
            //Syntax
        // HashMap <Typr, Typr> nameofvariable = new HashMap<>();

        // EXAMPLE
        HashMap<String, String> username = new HashMap<>();
        username.put("Karen", "Krivas123");
        username.put("Stephen", "squedea001");
        username.put("juan", "thrjuanandon210");
        username.put("Josh", "sleepy512");

        System.out.println(username);

        System.out.println(username.get("Juan"));

        System.out.println(username.get("Stephen"));


        // updating ourHashMap
        username.put("Jose", "Jose321");
        System.out.println(username);

        //replacing in our hash map
        username.replace("Stephen", "goat247");


        // remove pairs from hash map

        System.out.println(username.remove("Karen")); // krivas123

        System.out.println(username); //updates hash map that does not contain krivas123

        username.clear();
        System.out.println(username); // {}

        System.out.println(username.isEmpty()); // true





    }

}
