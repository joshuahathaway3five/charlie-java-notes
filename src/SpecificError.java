import Validation.Input;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SpecificError {
    public static void main(String[] args) {

        Scanner Input = new Scanner(System.in);

        int choice = Input.nextInt();

        int[] numbers = { 10, 11, 12, 13, 14, 15 };
        System.out.print("Please enter the index of the array: ");

        try {
            System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);
        }

        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error: Index is invalid");
        }
        catch (InputMismatchException a) {
            System.out.println("Error: You did not enter an integer.");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
