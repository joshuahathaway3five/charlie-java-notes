import java.util.Scanner;

public class Console {
    public static void main(String[] args) {


        //Printing out our data in the console / terminal

        String cohort = "Charlie";
        System.out.println(cohort);
        System.out.format("Hello there, %s. Nice to see you.\n", cohort);
        // System.out.printf (formattedstring, data);


        String school = "CodeBound";

        // Print ln = line

        System.out.format("Hello there, %s. Welcome to %s", cohort, school);
//        System.out.printf("Hi, my name is %s", "karen");


        // SCANNER CLASS - gets input from the console.

//        Scanner userInput = new Scanner(System.in);
//        System.out.println("Enter something"); // prompt the user to enter data
//        String input = userInput.nextLine(); // obtaining the value that the user inputted
//        System.out.println("Thank you, you entered : \n" + input); // "souting" what the user entered


          Scanner sc = new Scanner(System.in);
//        String userInput = sc.next();
//        String userInput2 = sc.next();
//        System.out.println(userInput);
//        System.out.println(userInput2);
//
//
//        // .next() captures each input separated by a string
//
//        System.out.println("Please enter your First, Middle and Last name: ");
//        String first = sc.next();
//        String middle = sc.next();
//        String last = sc.next();
//
//        System.out.println("My full name is " + first + "" + middle + "" + last);


//        .nextInt() captures the first valid int value
        System.out.println("How old are you?");
        int age = Integer.parseInt(sc.next()); // nextInt
        System.out.println(age);





    }



}
