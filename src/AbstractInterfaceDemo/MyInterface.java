package AbstractInterfaceDemo;

public interface MyInterface {

    int myInt = 5;

    public static String someStaticMethod() {
        System.out.println("This is a static method in my interface");
        return "";
    }

    public default void someDefaultMethod() {
        System.out.println("This is a default method in my interface");
    }


}
