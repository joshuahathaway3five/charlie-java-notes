package AbstractInterfaceDemo;

public class Main {
    public static void main(String[] args) {
//        Instantiate / create an instance of your MyClass class and create a new MyClass object

        MyClass x = new MyClass();

//        Create an instance of your MyClass class and create a new MyClass object
//        Using your instance variable name, call the someMethod() method that's in your MyClass
//        Also, print/sout the value of your myInt from your MyInterface

        MyClass y = new MyClass();

        y.someMethod();

        System.out.println(MyInterface.myInt);

        System.out.println(MyInterface.someStaticMethod());

        System.out.println();
    }
}
