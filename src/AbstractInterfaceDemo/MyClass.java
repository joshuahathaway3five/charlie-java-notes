package AbstractInterfaceDemo;

public class MyClass implements MyInterface {

    public void someMethod() {
        System.out.println("This is a method implemented in MyClass");
    }

}
