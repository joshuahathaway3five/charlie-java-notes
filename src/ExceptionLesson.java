// Syntax for try-catch-finally










public class ExceptionLesson {
    public static void main(String[] args) {

        //Example try.. catch block

        try {
            int divideByZero = 5 / 0;
            System.out.println("Rest of code in try block");
        }
        catch (ArithmeticException e) {
            System.out.println("ArithmeticException" + e.getMessage()); // display the defualt built in exception message for arithmeticexception
        }


        //EXAMPLE MULTIPLE CATCH BLOCKS
        class ListOfNumbers {
            public int[] arrayOfNumbers = new int[10];

            public void writeList() {
                try {
                    arrayOfNumbers[10] = 11;
                }
                catch (NumberFormatException e1) {
                    System.out.println("Number Format Exception" + e1.getMessage());
                }
                catch (IndexOutOfBoundsException e2) {
                    System.out.println("Index Out Of Bounds Exception" + e2.getMessage());
                }

            }
        } // end of numbers class

        ListOfNumbers list = new ListOfNumbers();
        list.writeList();

        // try-catch-finally
        try {
            int divideByZero = 5 / 0;
        }
        catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("Finally block is always executed");
        }
    }
}
