package CodeBoundGymProject;

public interface Calculator <T extends Number> {

    public abstract void calculateFees(double clubID);

}
