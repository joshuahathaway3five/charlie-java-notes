package CodeBoundGymProject;

public class SingleClubMember extends TheMemberClass{
    private int club;

    public SingleClubMember(String memberType, int memberID, String name, double fees, int club) {
        super(memberType, memberID, name, fees);
        this.club = club;

    }

    public int getClub() {
        return club;
    }
    public void setClub(int club) {
        this.club = club;
    }

    @Override
    public String toString() {
        return "memberType='" + this.getMemberType() + '\'' +
                ", memberID=" + this.getMemberID() +
                ", name='" + this.getName() + '\'' +
                ", fees=" + this.getFees() +
                "club=" + club +
                '}';
    }
}
