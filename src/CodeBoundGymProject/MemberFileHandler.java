package CodeBoundGymProject;
import InheritanceGym.Member;

import java.util.LinkedList;
import java.io.*;


public class MemberFileHandler {



    public LinkedList<TheMemberClass> readFile() throws FileNotFoundException {
        LinkedList<TheMemberClass> m = new LinkedList();
        String lineRead = null;
        String[] splitLine;
        TheMemberClass mem;


        BufferedReader Reader;

        try (BufferedReader reader = new BufferedReader(new FileReader("./CodeBoundGymProject/members.csv"))) {
            lineRead = reader.readLine();
            while (lineRead != null){
                splitLine = lineRead.split(", ");
                if (splitLine[0].equals("S"))
                {    mem = new SingleClubMember("S", Integer.parseInt(splitLine[1]), splitLine[2], Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));}
                else{    mem = new MultiClubMember("M", Integer.parseInt(splitLine[1]), splitLine[2], Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));}
                m.add(mem);
                lineRead = reader.readLine();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return m;
    }

    public void appendFile(String mem) throws IOException {
BufferedWriter writer = new BufferedWriter(
        new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/src/CodeBoundGymProject/members/csv")
);
    }

    public void overWriteFile( LinkedList<TheMemberClass> m) throws IOException {
    String s;

    BufferedWriter writer = new BufferedWriter(new FileWriter("members.csv", false));

    try {
        for (int i=0; i < m.size(); i++)
        {    s = m.get(i).toString();
            writer.write(s + "\n");}
    } catch (IOException IO){
        System.out.println(IO.getMessage());
    }
    }


}
