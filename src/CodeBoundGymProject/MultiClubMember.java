package CodeBoundGymProject;

public class MultiClubMember extends TheMemberClass{
    private int membershipPoints;


    public MultiClubMember(String memberType, int memberID, String name, double fees, int membershipPoints) {
        super(memberType, memberID, name, fees);
        this.membershipPoints = membershipPoints;
    }

    public int getMembershipPoints() {
        return membershipPoints;
    }
    public void setMembershipPoints(int membershipPoints) {
        this.membershipPoints = membershipPoints;


    }

    @Override
    public String toString() {
        return "memberType='" + this.getMemberType() + '\'' +
                ", memberID=" + this.getMemberID() +
                ", name='" + this.getName() + '\'' +
                ", fees=" + this.getFees() +
                "membershipPoints=" + membershipPoints +
                '}';
    }
}
