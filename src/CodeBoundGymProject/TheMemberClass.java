package CodeBoundGymProject;

public abstract class TheMemberClass {

   private String memberType;
   private int memberID;
   private String name;
   private double fees;


    public TheMemberClass(String memberType, int memberID, String name, double fees) {
        this.memberType = memberType;
        this.memberID = memberID;
        this.name = name;
        this.fees = fees;

    }

    @Override
    public String toString() {
        return "memberType='" + memberType + '\'' +
                ", memberID=" + memberID +
                ", name='" + name + '\'' +
                ", fees=" + fees +
                '}';
    }

    public String getMemberType() {
        return memberType;
    }
    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }



    public int getMemberID() {
        return memberID;
    }
    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }



    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }



    public double getFees() {
        return fees;
    }
    public void setFees(int fees) {
        this.fees = fees;
    }

}
