import java.util.HashMap;

public class CollectionsHashMapExercise {
    public static void main(String[] args) {

//        Create a program that will append a specified element to the end of a hash map.

        HashMap<String, String> append = new HashMap<>();
        append.put("Chicken", "Salad");
        append.put("Turkey", "Burger");


//    Create a program that will iterate through all the elements in a hash map


//        Create a program to test if a hash map is empty or not.
        HashMap<String, String> Empty = new HashMap<>();
        Empty.put("beep", "boop");
        System.out.println(Empty.isEmpty());


//        Create a program to get the value of a specified key in a map
        HashMap<String, String> Key = new HashMap<>();
        Key.put("Test", "testing");
        Key.put("Test2", "testing2");

        String val = (String)Key.get("Test");

        System.out.println("Value for Test is: " + val);


//        Create a program to test if a map contains a mapping for the specified key.

        HashMap<String, String> Key2 = new HashMap<>();
        Key2.put("Test", "testing");
        Key2.put("Test2", "testing2");

        System.out.println(Key2.get("Test"));



//        Create a program to test if a map contains a mapping for the specified value.


        HashMap < Integer, String > hash_map = new HashMap < Integer, String > ();
        hash_map.put(1, "Red");
        hash_map.put(2, "Green");
        hash_map.put(3, "Black");
        hash_map.put(4, "White");
        hash_map.put(5, "Blue");

        System.out.println("The Original map: " + hash_map);
        System.out.println("1. Is value 'Green' exists?");

        if (hash_map.containsValue("Green")) {
            System.out.println("Yes! ");
        } else {
            System.out.println("no!");
        }




//        Create a program to get a set view of the keys in a map

        HashMap <Integer, String> Key3 = new HashMap<>();
        Key3.put(1, "Red");
        Key3.put(2, "Green");
        Key3.put(3, "Black");
        Key3.put(4, "White");

        System.out.println(Key3.entrySet());


//        Create a program to get a collection view of the VALUES contained in a map.

        HashMap <Integer, String> Key4 = new HashMap<>();
        Key4.put(1, "Red");
        Key4.put(2, "Green");
        Key4.put(3, "Black");


        System.out.println("Collection view is: "+ Key4.values());


    }
}
