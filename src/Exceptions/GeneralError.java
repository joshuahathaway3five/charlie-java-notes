package Exceptions;

import java.util.Scanner;

public class GeneralError {

//    Create two integer variables, name them numerator, denominator.
//    Create a Scanner object name userInput and pass System.in as an argument.

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        int numerator = userInput.nextInt();

        int denominator = userInput.nextInt();

        try {
            System.out.println("Enter the Numerator" + numerator / denominator );

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("- - - End of Error Handling Example - - -");
        }

    }
}
