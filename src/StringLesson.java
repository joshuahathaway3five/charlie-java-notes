public class StringLesson {
    public static void main(String[] args) {
//        //     Strings
//        String message = "Hello World";
//        System.out.println(message);
//
////        String anotherMessage
//        String anotherMessage = "Another message assigned!";
//
////        CONCATENATION
////        System.out.println(message + " " + anotherMessage);
////        format
//
//        System.out.format("%s %s", message, anotherMessage);


        // STRING COMPARISON METHODS
       // .equals(), .equalsIgnoreCase(), .startWith(0, .endsWith()

//        String hero = "Batman";
//
//        if (hero == "Batman") { // DO NOT WANT TO DO THIS!... EVER!
//            System.out.println("nan nan nanananana");
//        }
//
//
//        // DOP THIS INSTEAD
//
//        if (hero.equals("batman")) {
//            System.out.println("nananana");

        String input = "charlie Rocks!";
        System.out.println(input.equals("charlie rocks!")); // False
        System.out.println(input.equals("Charlie rocks!")); // false
        System.out.println(input.equals("CHARLIE ROCKS!")); // false
        System.out.println(input.equals("Charlie Rocks")); // false

        System.out.println(input.equalsIgnoreCase("charlie rocks!")); //
        System.out.println(input.equalsIgnoreCase("Charlie rocks!")); //
        System.out.println(input.equalsIgnoreCase("CHARLIE ROCKS!")); //
        System.out.println(input.equalsIgnoreCase("Charlie Rocks")); //

        System.out.println(input.startsWith("C"));
        System.out.println(input.endsWith("s"));


        // STRING MANIPULATION METHODS\

//        char charAt; (;int; index);
        // charAt() - returns the character at the specififed index of the string

       // indexOf() - returns the index of the first ocurrence of a certain sub string
        // *returns -1 if the substring is not found

        // lastIndexOf() - like indexOf(), but start the search from the end of the string

        // length() - returns the length of the string

        // replace() - returns a copy of the string that has oldChar replaced with
        // newChar

        String myStr = "CodeBound";
        System.out.println(myStr.replace('B','Z'));

        // substring() - returns a new substring that starts at a specified index and
        //(optionally) end at the specified index


        String name = "Charlie";
        System.out.println(name.substring(1)); // harlie

        //toLowerCase() / toUpperCase()

        // trim() - returns a copy of the string without leading or trailing whitespace
    }
    }
