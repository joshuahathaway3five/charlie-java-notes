import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Collections;

public class CollectionsArraysExercise {
    public static void main(String[] args) {



//        Create a new class called CollectionsArraysExercise
//                Create a program that will print out an array of integers.
//        Create a program that will print out an array of strings
//        Create a program that iterates through all elements in an array. The array should hold the names of everyone in Charlie Class
//        Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
//                Create a program to remove the third element from an array list. You can create a new array or use a previous one.
//                Create an array of Dog breeds. Create a program that will sort the array list.
//                Create an array of cat breeds. Create a program that will search an element in the array List.
//        Now create a program that will reverse the elements in the array.


        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(25);

        System.out.println(numbers);


        ArrayList<String> roasts = new ArrayList<>(Arrays.asList(
                "light",
                "medium",
                "bold",
                "dark"

        ));

        System.out.println(roasts);

        ArrayList<String> names = new ArrayList<>(Arrays.asList(
                "Josh",
                "Jose",
                "Nick",
                "Stephen"

        ));

        Enumeration<String> e = Collections.enumeration(names);

        System.out.println(names);


        ArrayList<String> AddName = new ArrayList<>(Arrays.asList(
                "Josh",
                "Jose",
                "Nick",
                "Stephen"
        // only add the back not front
        ));


        AddName.add("Karen");
        System.out.println(AddName);

        ArrayList<String> Remove3rd = new ArrayList<>(Arrays.asList(
                "Josh",
                "Jose",
                "Nick",
                "Stephen"

        ));
        Remove3rd.remove(2);
        // removes Nick, the 3rd index
        System.out.println(Remove3rd);

        ArrayList<String> Dogs = new ArrayList<>(Arrays.asList(
                "Husky",
                "German Sheperd",
                "Golden Retriever",
                "Mut"

        ));

        System.out.println(Dogs);

        ArrayList<String> Cats = new ArrayList<>(Arrays.asList(
                "Cat",
                "Cat2",
                "Cat3",
                "StupidCat"

        ));
        System.out.println(Cats.indexOf("Cat2"));
        System.out.println();



    }
}
