package Validation;

import java.util.Scanner;

public class Input {

    private final Scanner scanner;

    public Input(Scanner scanner) {


        this.scanner = new Scanner(System.in);

    }


    Scanner getString() {
        System.out.println(scanner);
        return scanner;
    }


    public boolean yesNo() {
        System.out.println("Type YES or N)");
        String userInput = scanner.next().toUpperCase();
        if (userInput.equals("YES")) {
            System.out.println("True");
            return true;
        } else {
            System.out.println("False");
            return false;
        }

    }

    int getInt(int min, int max) {

        System.out.println("Please enter a number 1 - 10");
        int userInput = scanner.nextInt();
        if (userInput >= min && userInput <= max) {
            System.out.format("Your number is %s, good job!", userInput);
        return 0;
        } else {
            System.out.println("Invalid input");
            return getInt(min, max);
        }

    }

    int getInt() {
        int min = 1;
        int max = 10;
        System.out.println("Please enter a number 1 - 10");
        int userInput = scanner.nextInt();

        if (userInput >= min && userInput <= max) {
            System.out.format("Your number is %s, good job!", userInput);
            return 0;
        } else {
            System.out.println("Invalid input");
            return getInt();
        }
    }

    double getDouble(double min, double max) {
        return 0;
        // not done yet
    }



}
