import java.util.Scanner;

public class ConsoleExcersise {
    public static void main(String[] args) {

//        For the following exercises, create a new class named ConsoleExercise with a main method like the one in your HelloWorld class.
//        Copy this code into your main method:
//        double pi = 3.14159;
//        Write some java code that uses the variable pi to output the following:
//        The value of pi is approximately 3.14.
//                Don't change the value of the variable, instead, reference one of the links above and use System.out.format to accomplish this.


        float pi = (float) 3.14159;
        System.out.format("The value of pi is approximately %s", pi);

        int wholePi = (int) pi;
        System.out.format("The value of pi is approximately %s" + ".14", wholePi);


//        Explore the Scanner Class
//        Prompt a user to enter a integer and store that value in an int variable using the nextInt method.
//    * What happens if you input something that is not an integer?
//        Prompt a user to enter 3 words and store each of them in a separate variable, then display them back, each on a newline.
//                * What happens if you enter less than 3 words?
//    * What happens if you enter more than 3 words?
//

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please type a whole number");
        int age = Integer.parseInt(scanner.next());
        System.out.println(age);




        System.out.println("Please list you 3 foods you like");
        String userFood = scanner.nextLine();
        String userFood2 = scanner.nextLine();
        String userFood3 = scanner.nextLine();
        System.out.format("The 3 foods you like are" + " %s " + "and %s" + " and %s", userFood, userFood2, userFood3);


//        Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//                * do you capture all of the words?


        //System.out.println("Please Enter a sentence");
        //String userSentence = scanner.next();
        //System.out.format("you said... %s", userSentence );


 //       Rewrite the above example using the .nextLine method.

        System.out.println("Please Enter a sentence");
        String userSentence = scanner.nextLine();
        System.out.println(userSentence);




//        Calculate the perimeter and area of Code Bound's classrooms
//        Prompt the user to enter values of length and width of a classroom at Code Bound.
//        Use the .nextLine method to get user input and cast the resulting string to a numeric type.

        System.out.println("what is the Length and Width of the CodeBound classroom?");
        float UserWidth = Float.parseFloat(scanner.next());
        float UserLength = Float.parseFloat(scanner.next());

//                * Assume that the rooms are perfect rectangles.
//    * Assume that the user will enter valid numeric data for length and width.




//        Display the area and perimeter of that classroom.
//        The area of a rectangle is equal to the length times the width, and the perimeter of a rectangle is equal to 2 times the length plus 2 times the width.

        Float UserPerimeter = (UserLength * 2) + (UserLength * 2);
        Float UserArea = UserLength * UserWidth;
        System.out.format("The Perimeter is %s" + "and your area is %s", UserPerimeter, UserArea);


        //                Bonuses
//                * Accept decimal entries
//    * Calculate the volume of the rooms in addition to the area and perimeter








    }
}
