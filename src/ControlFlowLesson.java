import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {

        //              COMPARISON OPERATORS
        // ==, >, <, <= , >=, !=

        System.out.println(5 != 2); // true
        System.out.println(5 >= 5); // true
        System.out.println(5 <= 5); //true


//      Logical Operators
        // &&, ||

        System.out.println(5 == 6 && 2>1 && 3 != 3); // false
        System.out.println(5 != 6 && 2>1 && 3 != 3);
        System.out.println(5 != 6 && 2>1 && 3 == 3);


//         .equals() , equalsIgnoreCase() both are "==="

//          if statements
int score = 34;
if(score >= 50) {
    System.out.println("new high score");
} else {
    System.out.println("try again...");
}

//     Switch statements
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your grade");
        String userInput = scanner.nextLine();
        switch (userInput) {
            case "A":
                System.out.println("A");
                break;
            case "B":
                System.out.println("B");
                break;
            case "C":
                System.out.println("C");
                break;
            case "D":
                System.out.println("D");
                break;
            default:
                System.out.println("Invalid Input");
        }

        // WHILE LOOP
        int i = 0;
        while (i <= 10) {
            System.out.println("i is " + i);
            i++;
        }

        // DO WHILE LOOP
        do {
            System.out.println("you will see this!");
        } while (false);

//      FORLOOPS
for (var x = 1; x <= 10; x++) {
    System.out.println(x);
}


    }
}
