package InheritanceGym;

import java.util.Scanner;

public abstract class Member {

    public String Welcome = "Welcome to Inherit Fitness";
    protected double annualFee;
    private String name;
    private int memberID;
    private int memberSince;
    private int discount;


//- The first constructor with no parameters but prints in the console, "Parent Constructor with no parameters"
//            - The second constructor with the following parameters:
//            - name, memberID, and memberSince
//    - Also inside of the constructor, print(sout) "Parent Constructor with 3 parameters"


    public Member() {
        System.out.println("Parent Constructor with no parameters");
    }

    public Member(String name, int memberID, int memberSince) {
        this.name = name;
        this.memberID = memberID;
        this.memberSince = memberSince;
        System.out.println("Parent Constructor with 3 parameters");
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        Scanner getter = new Scanner(System.in);
        this.discount = discount;
        System.out.println("please enter the Admin password");
        int UserInput = getter.nextInt();
        if (UserInput == 123456) {
            System.out.println("Please enter the discount");
            int UserInput2 = getter.nextInt();
            discount = getter.nextInt();
        }

    }


    public void displayMemberInfo() {
        System.out.println(this.name);
        System.out.println(this.memberID);
        System.out.println(this.memberSince);
        System.out.println(this.annualFee);

    }

    public abstract void calculateAnnualFee();

}
