package Objects;

public class Cat {
    // instance variable: state(s) of the Cat class
    String name;
    int age;
    String color;
    String breed;

    //instant methods: "behaviors" of the cat
    public static void sleep() {
        System.out.println("this cat is now sleeping...");
    }
    public static void play() {
        System.out.println("this cat is now playing...");
    }
    public static void feed() {
        System.out.println("this cat is now eating...");
    }


    public String greeting() {
        return String.format("Hello this is %s", name);
    }
}
