//package Objects;
//
//public class StudentMain {
//    public static void main(String[] args) {
//        // create two new students
//        Student s1 = new Student("Karen");
//        Student s2 = new Student("Stephen", "Charlie");
//        Student s3 = new Student("AJ","Alpha, 89.9");
//
//        System.out.println(s1.getStudentInfo());
//        System.out.println(s2.getStudentInfo());
//
//        System.out.println(s3.name);
//        System.out.println(s3.grade); // cant access the private property of the class
//
//        System.out.println(s3.shareGrade());
//    }
//}
