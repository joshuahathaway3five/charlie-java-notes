package Objects;

public class Mathamatics {
    // Static fields are defined with the static keyword
    // - are shared by all instances of the class
    // - means that static properties should NOT be anything that is supposed to be UNIQUE to the instances of the class

    //static property
    public static double myPi = 3.14159;

    // static method
    public static int add(int x, int y) {
        return x + y;
    }

    public static int multiply(int x, int y) {
        return x * y;
    }
}
