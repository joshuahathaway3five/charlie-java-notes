package Objects;

public class Person {
    // this class has both static and instance properties
    public static long worldPop = 750000000L; // class property

    public String name; // instance property

    // psvm
    public static void main(String[] args) {
        Person theGreatestGuitaristEver = new Person();
        theGreatestGuitaristEver.name = "Eddie Van Halen";

        //access our static property
        Person.worldPop++;

        System.out.println(worldPop);

        System.out.println(theGreatestGuitaristEver.name);

    }


}
