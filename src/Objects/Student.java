package Objects;
public class Student {
    public String name;
    public String cohort;


    // private
    private double grade;


    // EXAMPLE OF ENCAPSULATION
    public Student(String studentName) {
        name = studentName;
        cohort = "Unassigned";

    }


//    public Student(String studentName, String assignedCohort) {
//        name = studentName;
//        cohort = assignedCohort;
//    }

//
//    public void StudentCohort(String cohort) {
//        // this keyword provides us a way to refer to the CURRENT instance.
//        this.cohort = cohort;
//        }

    public Student(String name, String cohort){
        this.name = name;
        this.cohort = cohort;
    }

    public Student(String name, String cohort, double grade) {
        this.name = name;
        this.cohort = cohort;
        this.grade = grade;
    }

    public String getStudentInfo() {
        return String.format("Student Name: %s\nCohort Assigned: %s\n", name, cohort);
    }

    public String sayHello() {
        return "Hello from" + this.name + "!";
    }

    public double shareGrade() {
        return grade;
    }
}
