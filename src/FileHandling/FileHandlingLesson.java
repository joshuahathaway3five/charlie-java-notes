package FileHandling;

// Main DFocus: File, BufferedReader, FileReader, BufferedWriter, and FileWriter from java.io package
// how to import?
// import java.io*;

// alternate libraries:
// Path Interface, paths class,
// Files class freom the java.io and java.nio packages


// File class
// - used for creating files and directories, file searching and file deletion

//BufferedReader class
//- reads text from a character-input stream,
//- buffering characters so as to provide for efficient reading of characters, arrays and lines
//Note: buffer size may ber specified or the default size may be used
//
//BufferedWriter class
//- Creates a buffered character-output stream that uses a default-sized output buffer
//-it inherits the Writer class
//
//FileWriter class
//- used to write character-oriented data to a file
//- you dont need to convert string into byte array
//- provides method to write strings directly
//
//FileReader Class
//-used to read data from the file
//- return data in byte format

//Paths:
//- absolute paths: specified from the filesystem  root
//- relative paths: relative to the CURRENT WORKING DIRECTORY

// EX
// /
//   users
//       codebound/
//           documents/
//                important-stuff.txt
//                      charlie-java-notes/   <-- current working directory
//                          src/

import java.io.*;
import java.nio.Buffer;

public class FileHandlingLesson {
    public static void main(String[] args) {

        // READING IF THERES A TEXT FILE WITH A NAME
        // use the FileReader class w/ a Buffererd Reader class

        //create a str4ing variable
        String line;

        //creaste a bufferedReader object
        BufferedReader bufferedReader = null;

        //try-catch-finally statenebt
        try {
            bufferedReader = new BufferedReader(new FileReader("src/FileHandling/movieQuotes/hamlet.txt"));

            line = bufferedReader.readLine();

            while(line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        finally { // finally runs no matter what
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }
            catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }


//         WRITING TO A TEXT FILE...
//         use FileWriter class w/ a BufferedWriter class

//         create a string variable

        String text = "I'll be back";

        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("src/FileHandling/movieQuotes/terminator.txt")
        ))
        {
            writer.write(text);
            writer.newLine();
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }



        // OVERWRITING A TEXT FILE / FILE'S TEXT...

        // create a string variabler
//        String overwriteText = "Listen to many, speak to few";
//        try(BufferedWriter rewrite = new BufferedWriter(new FileWriter("src/FileHandling/movieQuotes/hamlet.txt"))) {
//
//            rewrite.write(overwriteText);
//            rewrite.newLine();
//        }
//        catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
//
//
//         // RENAMING A TEXT FILE
//        File oldFileName = new File("src/Filehandling/movieQuotes/terminator./txt");
//        // targeting the file we want to rename
//
//        File newFileName = new File("src/FileHandling/movieQuotes/terminator2.txt");
//        // creating the name that we want
//
//        oldFileName.renameTo(newFileName);


        // DELETING A TEXT FILE
        File newFileName = new File("src/FileHandling/movieQuotes/terminator2.txt");

        newFileName.delete();
        // cli: 'rm -rf fileName'


        String Artist1 = "DaftPunk";
        String Year1 = "2011";
        String Genre1 = "Electric";

        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/Albums/DaftPunk-Tron")
        ))
        {
            writer.write(Artist1);
            writer.newLine();
            writer.write(Year1);
            writer.newLine();
            writer.write(Genre1);

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }

        String Artist2 = "Blackmill";
        String Year2 = "2012";
        String Genre2 = "Dubstep";

        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/Albums/Blackmill-Miracle")
        ))
        {
            writer.write(Artist2);
            writer.newLine();
            writer.write(Year2);
            writer.newLine();
            writer.write(Genre2);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }

        String Artist3 = "DoctorVox";
        String Year3 = "2014";
        String Genre3 = "Electric";

        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/Albums/DoctorVox-Frontier")
        ))

        {
            writer.write(Artist3);
            writer.newLine();
            writer.write(Year3);
            writer.newLine();
            writer.write(Genre3);

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        /////////////////////////////////////////

        // DATA

        //create five (5) new text files
//        The name of each file name should be a name of a president (past or present)
//        Inside of your text files, you should include:
//        - Name of the president
//                - The number of that president
//        - Their political party
//        - Years served
//                - One famous quote from that president

//        class name {
//            String Name = "";
//        }

        String Name1 = "GeorgeWashington";
        int Num1 = 1;
        String Party1 = "None/Federalist";
        String Term1 = "2 terms (8 years)";
        String Quote1 = "It is better to offer no excuse than a bad one.";


        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/data/FileHandlingLesson/GeorgeWashington")
        ))

        {
            writer.write(Name1);
            writer.newLine();
            writer.write(Num1);
            writer.newLine();
            writer.write(Party1);
            writer.newLine();
            writer.write(Term1);
            writer.newLine();
            writer.write(Quote1);

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }




        String Name2 = "AbrahamLincoln";
        int Num2 = 16;
        String Party2 = "National Union (republican)";
        String Term2 = "1 terms (4 years, Assassinated)";
        String Quote2 = "You can fool all the people some of the time, and some of the people all the time, " +
                "but you cannot fool all the people all the time. ...";


        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/data/FileHandlingLesson/AbrahamLincoln")
        ))

        {
            writer.write(Name2);
            writer.newLine();
            writer.write(Num2);
            writer.newLine();
            writer.write(Party2);
            writer.newLine();
            writer.write(Term2);
            writer.newLine();
            writer.write(Quote2);

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }


        String Name3 = "John F Kennedy";
        int Num3 = 35;
        String Party3 = "Democratic";
        String Term3 = "1 terms (2 years, Assassinated)";
        String Quote3 = "Ask not what your country can do for you, but what you can do for your country.";


        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/data/FileHandlingLesson/John-F-Kennedy")
        ))

        {
            writer.write(Name3);
            writer.newLine();
            writer.write(Num3);
            writer.newLine();
            writer.write(Party3);
            writer.newLine();
            writer.write(Term3);
            writer.newLine();
            writer.write(Quote3);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }



        String Name4 = "Franklin D. Roosevelt";
        int Num4 = 32;
        String Party4 = "Democratic";
        String Term4 = "4 terms (16 years)";
        String Quote4 = "speak softly and carry a big stick; you will go far.";


        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/data/FileHandlingLesson/Franklin-D-Roosevelt")
        ))

        {
            writer.write(Name4);
            writer.newLine();
            writer.write(Num4);
            writer.newLine();
            writer.write(Party4);
            writer.newLine();
            writer.write(Term4);
            writer.newLine();
            writer.write(Quote4);

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }


        String Name5 = "Thomas Jefferson";
        int Num5 = 3;
        String Party5 = "Democratic-Republican";
        String Term5 = "2 terms (8 years)";
        String Quote5 = "Never trouble another for what you can do yourself.";


        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("/Users/student02/IdeaProjects/Charlie-Java-Notes/data/FileHandlingLesson/Thomas-Jefferson")
        ))

        {
            writer.write(Name5);
            writer.newLine();
            writer.write(Num5);
            writer.newLine();
            writer.write(Party5);
            writer.newLine();
            writer.write(Term5);
            writer.newLine();
            writer.write(Quote5);

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }
}
