import java.util.Arrays;

public class StringMethodsExercise {
    public static void main(String[] args) {

//        EXERCISE STRING METHODS
//        Create a new class named StringMethodsExercise with a main method.
//        Copy this code into your main method.
//        Write some java code that will display the length of the string

        String myLength = "Good afternoon, good evening, and good night";
        int nameLength = myLength.length();
        System.out.println("The name " + myLength + " contains " + nameLength + "letters.");






        //        Use the some string, but instead of using 'int myLength', use 'String uCase'
//        Write some java code that will display the entire string in all uppercase
//        Use the same code but instead of using the toUpperCase() method, use the toLowerCase() method.
//                Print it out in the console, what do you see?


        String uCase = "Good afternoon, good evening, and good night".toUpperCase();
        int uCaseLength = uCase.length();
        System.out.println(uCase);

        String LCase = "Good afternoon, good evening, and good night".toLowerCase();
        int LCaseLength = LCase.length();
        System.out.println(LCase);



//        Copy this code into your main method
//        String firstSubstring = "Hello World".substring(6);
//        Print it out in the console, what do you see?

        String firstSubstring = "Hello World".substring(3);
        System.out.println(firstSubstring);


//        Change the argument to 3, print out in the console. What do you get?
//        Change the argument to 10, print it out in the console. What do you get?


//        Create a String variable name 'alpha' and assign all the names in the cohort in ONE string.
//                Using the split() method, create another variable named 'splitAlpha' where it displays the list of names separated by a comma.
//                Print out your results in the console
//        Ex. [Peter, John, Andy, David];

        String alpha = "NIck Josh Jose Stephen Karen";
        String[] splitAlpha = alpha.split(" ");
        System.out.println(Arrays.toString(splitAlpha));









//        Use the following string variables.
//                String m1 = "Hello, ";
//        String m2 = "how are you?";
//        String m3 = "I love Java!"
//        Using concatenation, print out all three string variables that makes a complete sentence.




    }
}
