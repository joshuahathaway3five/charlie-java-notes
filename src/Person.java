public class Person {
    // instance variable
    private String name;


    // Contructor with no parameters
    public Person() {

    }

    // constructor with one parameter
    public Person(String name) {

    }
    // instance method
    public String getInfo(){
        return "my name is" + name;
    }

    // getters / setters methods
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Person person1 = new Person("Josh");
        System.out.println(person1.getName()); // Josh

        person1.setName("Nick");
        System.out.println(person1.name); // Nick
    }

}
