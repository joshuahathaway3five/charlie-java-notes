//public class MethodLesson {
//    public static void main(String[] args) {

//        sayHello(3);   // World Hello
//                            // World Hello
//                            // World Hello
//        sayHello("Charlie"); // Charlie Hello
//
//        sayHello("Jose", "Hey"); // Hey Jose


//        System.out.println(greeting("Charlie"));
//
//        sayHi("hey there", "josh"); {
//
//        }
//
//        returnFive(); {
//
//        }
//
//        System.out.println(yelling("im not yelling!"));

//
//
//    // syntax of defining a method
//
////    public static returnType methodName(param1, param2, param3...) {
////        the statement(s) of the method
////        a return statement
////    }
//
//    public static String greeting(String name) {
//        return String.format("Hello there, %s", name);
//    }
//    // public - this is the visability modifier
//    //static - the presence of this keyword defines that the method belongs
//    //to the class, as opposed to instances of it.
//
//    // String - the return type of method
//    // greeting - name of the method
//
//    public static void sayHi(String greeting, String name) {
//        System.out.printf("%s %s!\n", greeting, name);
//    }
//
//
//    public static int returnFive() {
//        System.out.println("this is calling out returnFive()");
//        System.out.println(5);
//        return 5;
//
//    }
//
//
//    public static String yelling(String s) {
//        System.out.println(returnFive());
//        return s.toUpperCase() + "!";
//    }

    // MEATHOD OVERLOADING
    // - defining multiple methods with the same name, but with different parameter types, parameter order, or number of parameters


//    // V1
//    public static void sayHello(int num) {
//        for (int i = 0; i < num; i++) {
//            sayHello("Charlie");
//        }
//    }
//
//    // V2
//    public static void sayHello() {
//        sayHello("Hello", "World");
//    }
//
//    // V3
//    public static void sayHello(String name) {
//        sayHello("Hello", name);
//    }
//
//    // V4
//    public static void sayHello(String name, String greeting) {
//        System.out.println(greeting + "" + name);
//    }

public class MethodLesson {
    public static void main(String[] args) {

   String changeMe = "Hello CodeBound";
   // call our change this method
        changeThis(changeMe);

        System.out.println();

        System.out.println(changeMe);


    int result = addNums(10);
        System.out.println(result);


    }

    public static void  changeThis(String sentence) {
        sentence = "Dun nuh nunununuh";
    }

    // use recursion to add all of the numbers up to 10
    public static int addNums(int num){
        if (num > 0){
            return num + addNums(num - 1);
        }
        else {
            return 0;
        }

    }


    } // end of class
