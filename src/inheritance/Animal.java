package inheritance;

public class Animal {
    // INHERITANCE - one of the key concepts in OOP
    // we can reuse code
    // OOP really allows us to create classes to inherit the behavior from other classes

        // FIELDS / variables/ properties
        // - can be declared as public, private, or protected
        // creating the characteristics of things all animals have


    private String name;
    private  int brain;
    private int body;
    private int size;
    private int weight;



    // CONSTRUCTOR
    public Animal(String name, int brain, int body, int size, int weight) {
        this.name = name;
        this.brain = brain;
        this.body = body;
        this.size = size;
        this.weight = weight;
    }





    // method
    public void eat() {
        System.out.println("This animal is eating");
    }

    public void move() {
        System.out.println("This animal is moving");
    }




    // GETTER / SETTER METHODS
    // WHat / how are fields declared
    // - our fields are delcared as private
    // means the fields cannot be accessed out of this class


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrain() {
        return brain;
    }

    public void setBrain(int brain) {
        this.brain = brain;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    // GETTER AND SETTER METHODS CREATED
    // - now the private fields can be accessed by other classes
}
