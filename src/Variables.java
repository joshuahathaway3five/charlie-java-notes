/*Create a new class name Variables.java and complete the following:
        You should complete all of the following inside of your main method from the previous exercise. After each step, compile and run your code.
        Create an int variable named favoriteNum and assign your favorite number to it, then print it out to the console.
        Create a String variable named myName and assign your first name as a string value to it, then print the variable out to the console.
        Change your code to assign a character value to myName. What do you notice?
        Change your code to assign the value 3.14159 to myName. What happens?
        Declare an long variable named myNum, but do not assign anything to it. Next try to print out myNum to the console. What happens?
        Change your code to assign the value 3.14 to myNum. What do you notice?
        Change your code to assign the value 123L (Note the 'L' at the end) to myNum.
        Change your code to assign the value 123 to myNum.
        Why does assigning the value 3.14 to a variable declared as a long not compile, but assigning an integer value does?
        Change your code to declare myNum as a float. Assign the value 3.14 to it. What happens? What are two ways we could fix this?
        Copy and paste the following code blocks one at a time and execute them*/



public class Variables {
    public static void main(String[] args) {


        int favoriteNum;
        favoriteNum = 8;
        System.out.println(favoriteNum);

        String myName = "Josh";
        System.out.println(myName);

        float myNum = (float) 3.14;
        System.out.println(myNum);





        int x = 10;
        System.out.println(x++);
        System.out.println(x);

        int y = 10;
        System.out.println(++y);
        System.out.println(y);







//        What is the difference between the above code blocks? Explain why the code outputs what it does.
//        Try to create a variable named class. What happens?
//        Object is the most generic type in Java. You can assign any value to a variable of type Object. What do you think will happen when the following code is run?

//                int eight = number;
//                String number = "eight";


//        What are the two different types of errors we are observing?
//        Rewrite the following expressions using the relevant shorthand assignment operators:



//        Copy and paste the code above and then run it. Does the result match with your expectation?
//        How is the above example different from this code block?






//        What happens if you assign a value to a numerical variable that is larger (or smaller) than the type can hold? What happens if you increment a numeric variable past the type's capacity?
//        Hint: Integer.MAX_VALUE is a class constant (we'll learn more about these later) that holds the maximum value for the int type.

        int z = Integer.MAX_VALUE + 2;
        System.out.println(z);
    }
}
