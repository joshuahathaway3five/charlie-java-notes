package ArraysLesson;

public class Arrays {
    /*
  In Java arrays are a different kind of object that contains zero or more items called elements
  Array elements can be any valid type but all elements must be of the same type

  */

    // Syntax:
    // type[] nameOfVariable

    double[] prices;

    // in Java arrays have a fixed length, this is access by using the .length property, they have top be defined when they are created

    //the sizew of the array can either be a literal, const or a variable
    // arrays are 0 index

    public static void main(String[] args) {
        // example
        String[] developer = new String[5];
        developer[0] = "josh";
        developer[1] = "Nick";
        developer[2] = "Jose";
        developer[3] = "Stephen";
        developer[4] = "Karen";

        System.out.println(developer[3]); // stephen
        System.out.println(developer.length); // 5

        // Assigning a variable we created a new array where the size is determined by a constant defined before

//        int numOfBugs = (int) Math.floor(Math.random() * 100);
//        Bug[] myCode = new Bug[numOfBugs];

        int[] numbers = new int[3];
        numbers[0] = 11;
        numbers[1] = 12;
        numbers[2] = 13;

        // ArrayIndexOutOfBoundsException
        numbers[3] = 14;
        System.out.println(numbers[3]);

        //iterating
        //like js, we can use the length property of any array in combo with looping contruct object(s) to iterate it
        String[] lang = {"html", "css", "javascript", "jquery", "angular", "java"};
        for (int i = 0; i < lang.length; i++) {
            System.out.println(lang[i]);
        }

        // alter / "enhanced for" (java ForEach)
        String[] gameConsoles = {"ps4", "xbox1", "nintendo switch", "sega"};
        for (String gc : gameConsoles) {
            System.out.println(gc);
        }

        //two-dimensional array
        //"matrix" / grid-like array

        int[][] myMatrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9,}
        };

        // access the first element inside of the second row
        System.out.println(myMatrix[1][0]); // 4
        //last element of first row
        System.out.println(myMatrix[0][2]); // 3


        //iterating through the matrix using a nested loop targetting the first array

        for(int[] row : myMatrix) { // first loop
            System.out.println("+---+---+---+");
            System.out.println("|");

            for (int n : row) { // second loop targeting the second array
                System.out.println(n + "|");
            }
            System.out.println();
        }
        System.out.println("+---+---+---+");
    }
}
