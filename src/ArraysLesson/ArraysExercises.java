package ArraysLesson;

import Objects.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.io.*;
import java.lang.*;
import java.util.*;

public class ArraysExercises {

    static void reverse(Integer[] a)
    {
        Collections.reverse(Arrays.asList(a));
        System.out.println(Arrays.asList(a));
    }



        public static String[] addX(int n, String[] arr, String x)
        {
//
//            // create a new array of size n+1
          String[] newarr = new String[Integer.parseInt(n + "")];
//
//            // insert the elements from
//            // the old array into the new array
//            // insert all elements till n
//            // then insert x at n+1
//
           newarr[Integer.parseInt(String.valueOf(n))] = x;
//
           return newarr;

        }








    public static void main(String[] args) {

        int[] numbers = new int[]{1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(numbers));


//    Given an array of ints, return true if 6 appears as either the first or last element in the array.
        int[][] sixTrue = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
        };
        if (sixTrue[1][0] == 6) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }

        //    Given an array of ints length 3, return the sum of all the elements.

        System.out.println("Enter the required size of the array ");
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int[] myArray = new int[size];
        int sum = 0;
        System.out.println("Enter " + size + " elements of the array one by one ");

        for (int i = 0; i < size; i++) {
            myArray[i] = s.nextInt();
            sum = sum + myArray[i];
        }
        System.out.println("Elements of the array are " + Arrays.toString(myArray));
        System.out.println("Sum of the elements of the array are " + sum);


        //    Given an array of ints length 3, return a new array with the elements in reverse order


        {
            Integer[] arr = {1, 2, 3, 4, 5};
            reverse(arr);
        }

        // Create an array that holds 3 Person objects. Assign a new instance of the Person class to each element.
        // Iterate through the array and print out the name of each person in the array.


//        Person [] people = new Person[3];
//        new Person( = "I'm");
//        new Person( = "Pickle";
//        new Person() = "Rick";
//
//
//
//    }
//    public static Person[] addPerson(Person[] people, Person person) {
//        Person[] arrayCopy = Arrays.copyOf(people,people.length + 1);
//            arrayCopy[arrayCopy.length - 1] = person;
//
//            return arrayCopy;
//
//    }


//        Server Name Generator
//        We are going to build a server name generator. Create a class inside of src named ArraysLesson.ServerNameGenerator, and follow the specs below.
//    * Create two arrays whose elements are strings, one with at least 10 adjectives, another with at least 10 nouns.
//                * Create a method that will return a random element from an array of strings.
//                * Add a main method, and inside of your main method select and random noun and adjective and hyphenate the combination and display the generated name to the user.

        int n = 10;

        int i;

        String [] adjectives = new String[11];

        String [] nouns = new String[11];

        nouns [0] = "car";
        nouns [1] = "truck";
        nouns [2] = "van";
        nouns [3] = "tank";
        nouns [4] = "plane";
        nouns [5] = "rocket";
        nouns [6] = "ship";
        nouns [7] = "bike";
        nouns [8] = "wheelchair";
        nouns [9] = "skateboard";


        adjectives [0] = "blue";
        adjectives [1] = "fast";
        adjectives [2] = "crash";
        adjectives [3] = "bash";
        adjectives [4] = "trash";
        adjectives [5] = "slash";
        adjectives [6] = "hash";
        adjectives [7] = "stash";
        adjectives [8] = "thrash";
        adjectives [9] = "splash";
        adjectives [10] = "";



        System.out.println("Initial Array:\n"
                + Arrays.toString(adjectives));

        Scanner sc = new Scanner(System.in);
        // element to be added
        String x = sc.nextLine();

        // call the method to add x in arr
        adjectives = addX(n, adjectives, x);

        // print the updated array
        System.out.println("\nArray with " + x
                + " added:\n"
                + Arrays.toString(adjectives));

        // finally runs no matter what
    }

}

